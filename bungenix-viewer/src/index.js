import React from 'react';
import PdfDocumentViewer from './PdfViewer';
import NonPdfDocumentViewer from './NonPdfDocumentViewer';


class BungeniXViewer extends React.Component {
  
    render() {
      switch (this.props.format){
        case 'PDF': return <PdfDocumentViewer {...this.props}/>;
        case 'PNG': return <NonPdfDocumentViewer {...this.props}/>;
        case 'DOCX': return <NonPdfDocumentViewer {...this.props}/>;        
        case 'XML': return <NonPdfDocumentViewer {...this.props}/>;
        default : return "Format not supported";
      } 
    }
  }


export default BungeniXViewer;

