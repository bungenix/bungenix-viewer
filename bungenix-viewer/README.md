# BungeniX-Viewer

Viewer for PDF, DOCX, HTML, PNG documents, used by BungeniX-UI

> 

[![NPM](https://img.shields.io/npm/v/bungenix-viewer.svg)](https://www.npmjs.com/package/bungenix-viewer) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save bungenix-viewer
```

## Usage

```jsx
import BungeniXViewer from 'bungenix-viewer';

class Example extends Component {
  render () {
    
    const attLink = "http://domain/file.pdf"; // url to online pdf document
    const {doc} = this.props ; // AKN document as provided by bungenix-ui
    const formatUc = "PDF" ; // PDF, PNG, DOCX, XML

    return (
      <BungeniXViewer attLink={attLink} doc={doc} format={formatUc} />  
    )
  }
}
```

## License

LGPL-3.0 © [kohsah](https://github.com/kohsah)
